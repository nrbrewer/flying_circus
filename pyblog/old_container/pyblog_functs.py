from datetime import datetime
import os
import requests
import base64
import html2text


WORDPRESS_USERNAME = os.environ.get('WORDPRESS_USERNAME')
WORDPRESS_PASSWORD = os.environ.get('WORDPRESS_PASSWORD')
WORDPRESS_URL = os.environ.get('WORDPRESS_URL')
current_time = datetime.today().strftime('%Y%m%d%H%M%S')


def create_creds(user, password):
    creds = user + ':' + password
    token = base64.b64encode(creds.encode())
    header = {'Authorization': 'Basic ' + token.decode('utf-8)')}
    return header


def read_file(filename):
    try:
        with open(filename, 'r') as f:
            lines = f.readlines()
            title = ''.join(lines[0].split('\n'))
            content = '\n'.join(lines[2:])
        return title, content
    except FileNotFoundError as err:
        print('Please enter a valid filename:', err)
        exit()


def read_blog():
    response = requests.get(WORDPRESS_URL)
    if response.status_code != 200:
        print('read_blog API call failed!', response.status_code)
        return
    response = response.json()
    print('Read Successful!')
    render_output(response)
    return


def write_blog(header, filename):
    blog_title, blog_content = read_file(filename)
    json_post = {'title': blog_title,
                 'status': 'publish',
                 'content': blog_content}
    response = requests.post(WORDPRESS_URL, headers=header, json=json_post)
    response = [response.json()]
    print('Upload Successful!')
    render_output(response)
    return


def parse_args(args):
    filename = args['--file']
    command = args['<command>']
    return filename, command


def determine_call(header, command, filename):
    if command == 'upload':
        write_blog(header, filename)
    else:
        read_blog()
    return


def render_output(response):
    date = response[0]['date']
    title = response[0]['title']['rendered']
    content = html2text.html2text(response[0]['content']['rendered'])
    content = ''.join(content.split('\n'))
    print(date)
    print(title)
    print(content)
    return
