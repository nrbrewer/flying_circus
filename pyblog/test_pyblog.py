import pytest
from pyblog_functs import *


class MockResponse:
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        return self.json_data


def test_create_creds():
    """ Testing building of header creds """
    user = 'user'
    password = 'password'
    header = create_creds(user, password)
    print(header)
    assert header == {'Authorization': 'Basic dXNlcjpwYXNzd29yZA=='}


def test_read_file():
    """ Testing buildout of title and content """
    filename = 'blog_input_test'
    title, content = read_file(filename)
    print(title, content)
    assert title == 'Title'
    assert content == 'Test content\n'


def test_read_file_failure():
    """ Testing for File Not Found and System Exit """
    with pytest.raises(SystemExit) as sys_exit:
        filename = 'invalid_file'
        read_file(filename)
    assert FileNotFoundError
    assert sys_exit.type == SystemExit


def mock_read_blog_success(*args, **kwargs):
    """ Mocking a successful API call """
    print('Calling mock_read_blog requests.get')
    data = [{"id":"16",
                "date":"2020-12-09T19:28:56",
                "title":{"rendered": "Title"},
                "content": {"rendered": "Content is amazing"}
             }]
    response = MockResponse(json_data=data, status_code=200)
    return response


def mock_read_blog_not_200(*args, **kwargs):
    data = {}
    response = MockResponse(json_data=data, status_code=500)
    return response


def test_read_blog_returns_values(mocker):
    mocker.patch('requests.get', mock_read_blog_success)
    url = 'http://mock.testing/url' # Some string, can be anything
    blog_data = read_blog(url)
    assert blog_data is None


def test_read_blog_returns_not_200(mocker):
    mocker.patch('requests.get', mock_read_blog_not_200)
    url = 'http://mock.testing/url' # Some string, can be anything
    blog_data = read_blog(url)
    assert blog_data is None


def test_check_stdin_correct_format(mocker):
    mock_stdin = mocker.patch('sys.stdin')
    mock_stdin.read.return_value = 'This is a test title.\n\nThis is test content.\n'
    stdin_title, stdin_content = check_stdin()
    assert stdin_title == 'This is a test title.\n'
    assert stdin_content == 'This is test content.\n'


def test_check_stdin_invalid_format(mocker):
    mock_stdin = mocker.patch('sys.stdin')
    mock_stdin.read.return_value = 'This is a test title.  This is test content.'
    stdin_title, stdin_content = check_stdin()
    assert stdin_title != 'This is a test title.\n'
    assert stdin_content != 'This is test content.\n'


def test_check_stdin_empty(mocker):
    with pytest.raises(SystemExit) as sys_exit:
        mock_stdin = mocker.patch('sys.stdin')
        mock_stdin.read.return_value = ''
        stdin_title, stdin_content = check_stdin()
    assert sys_exit.type == SystemExit


def mock_write_blog_success(*args, **kwargs):
    """ Mocking a successful API call """
    print('Calling mock_write_blog requests.get')
    data = {"id":"16",
                "date":"2020-12-09T19:28:56",
                "title":{"rendered": "Title"},
                "content": {"rendered": "Content is amazing"}
             }
    response = MockResponse(json_data=data, status_code=200)
    return response


def test_write_blog_success_with_filename(mocker):
    mocker.patch('requests.post', mock_write_blog_success)
    header = {'Authorization': 'Basic dXNlcjpwYXNzd29yZA=='}
    filename = 'blog_input_test'
    url = 'http://mock.testing/url' # Some string, can be anything
    blog_data = write_blog(header, filename, url)
    assert blog_data is None


def test_write_blog_success_with_stdin(mocker):
    mock_stdin = mocker.patch('sys.stdin')
    mocker.patch('requests.post', mock_write_blog_success)
    header = {'Authorization': 'Basic dXNlcjpwYXNzd29yZA=='}
    filename = ''
    url = 'http://mock.testing/url' # Some string, can be anything
    blog_data = write_blog(header, filename, url)
    assert blog_data is None


def mock_write_blog_failure_with_not_200(*args, **kwargs):
    data = {}
    response = MockResponse(json_data=data, status_code=500)
    return response


def test_write_blog_failure_with_not_200(mocker):
    mocker.patch('requests.post', mock_write_blog_failure_with_not_200)
    header = {'Authorization': 'Basic dXNlcjpwYXNzd29yZA=='}
    filename = 'blog_input_test'
    url = 'http://mock.testing/url' # Some string, can be anything
    blog_data = write_blog(header, filename, url)
    assert blog_data is None


def test_parse_args_upload_with_filename():
    args = {'--file': 'blog_input_test', '<command>': 'upload',
            '--search': None}
    filename, command, search = parse_args(args)
    assert args['--file'] == 'blog_input_test'
    assert args['<command>'] == 'upload'


def test_parse_args_upload_without_filename():
    args = {'--file': None, '<command>': 'upload',
            '--search': None}
    filename, command, search = parse_args(args)
    assert args['--file'] == None
    assert args['<command>'] == 'upload'


def test_parse_args_without_any_options():
    args = {'--file': None, '<command>': None,
            '--search': None}
    filename, command, search = parse_args(args)
    assert args['--file'] == None
    assert args['<command>'] == None


def test_determine_call_not_read(mocker):
    mock_read = mocker.patch('pyblog_functs.read_blog')
    command = None
    header = {'Authorization': 'Basic dXNlcjpwYXNzd29yZA=='}
    filename = 'blog_input_test'
    search_term = ''
    determine_call(header, command, filename, search_term)
    url = 'Nonewp-json/wp/v2/posts?per+page=1'
    mock_read.assert_called_with(url)


def test_determine_call_upload(mocker):
    mock_write = mocker.patch('pyblog_functs.write_blog')
    command = 'upload'
    header = {'Authorization': 'Basic dXNlcjpwYXNzd29yZA=='}
    filename = 'blog_input_test'
    search_term = ''
    determine_call(header, command, filename, search_term)
    url = 'Nonewp-json/wp/v2/posts'
    mock_write.assert_called_with(header, filename, url)

def test_determine_call_search(mocker):
    mock_write = mocker.patch('pyblog_functs.search_blog')
    command = 'search'
    header = {'Authorization': 'Basic dXNlcjpwYXNzd29yZA=='}
    filename = 'blog_input_test'
    search_term = ''
    determine_call(header, command, filename, search_term)
    mock_write.assert_called_with(search_term)


def mock_search_blog_success(*args, **kwargs):
    pass
