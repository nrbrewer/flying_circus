from datetime import datetime
import sys
import os
import requests
import base64
import html2text


WORDPRESS_USERNAME = os.environ.get('WORDPRESS_USERNAME')
WORDPRESS_PASSWORD = os.environ.get('WORDPRESS_PASSWORD')
WORDPRESS_URL = os.environ.get('WORDPRESS_URL')
current_time = datetime.today().strftime('%Y%m%d%H%M%S')


def create_creds(user, password):
    creds = user + ':' + password
    token = base64.b64encode(creds.encode())
    header = {'Authorization': 'Basic ' + token.decode('utf-8)')}
    return header


def check_stdin():
    stdin = sys.stdin.read()
    if not stdin:
        print('No input given.')
        exit()
    stdin = stdin.split('\n')
    stdin_title = '\n'.join(stdin[:2])
    stdin_content = '\n'.join(stdin[2:])
    return stdin_title, stdin_content


def read_file(filename):
    try:
        with open(filename, 'r') as f:
            lines = f.readlines()
            title = ''.join(lines[0].split('\n'))
            content = '\n'.join(lines[2:])
        return title, content
    except FileNotFoundError as err:
        print('Please enter a valid filename:', err)
        exit()


def read_blog(url):
    response = requests.get(url)
    if response.status_code != 200:
        print('read_blog API call failed!', response.status_code)
        return
    response = response.json()
    if type(response) != list:
        response = [response]
    print('Read Successful!')
    render_output(response)
    return


def write_blog(header, filename, url):
    if not filename:
        blog_title, blog_content = check_stdin()
    else:
        blog_title, blog_content = read_file(filename)
    json_post = {'title': blog_title,
                 'status': 'publish',
                 'content': blog_content}
    response = requests.post(url, headers=header, json=json_post)
    if response.status_code != 200 and response.status_code != 201:
        print('write_blog API call failed!', response.status_code)
        return
    response = [response.json()]
    print('Upload Successful!')
    render_output(response)
    return


def search_blog(search_term):
    response = requests.get(str(WORDPRESS_URL) +
                            'wp-json/wp/v2/search?' +
                            f'search={search_term}&per_page=1')
    if response.status_code != 200:
        print('search_blog API call failed!', response.status_code)
        return
    response = response.json()
    blog_id = response[0]['id']
    print('Search Successful!')
    url = str(WORDPRESS_URL) + f'wp-json/wp/v2/posts/{blog_id}'
    read_blog(url)
    return


def parse_args(args):
    filename = args['--file']
    command = args['<command>']
    search_term = args['--search']
    return filename, command, search_term


def determine_call(header, command, filename, search_term):
    if command == 'upload':
        url = str(WORDPRESS_URL) + 'wp-json/wp/v2/posts'
        write_blog(header, filename, url)
    elif command == 'search':
        search_blog(search_term)
    else:
        url = str(WORDPRESS_URL) + 'wp-json/wp/v2/posts?per+page=1'
        read_blog(url)
    return


def render_output(response):
    date = response[0]['date']
    title = response[0]['title']['rendered']
    content = html2text.html2text(response[0]['content']['rendered'])
    print(date)
    print(title, '\n')
    print(content)
    return
