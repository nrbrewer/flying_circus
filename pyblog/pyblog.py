#! /usr/bin/env python3
"""PyBlog
Usage:
    pyblog.py [<command> --file=<filename> --search=<search_term>]
    pyblog.py [--version]

Options:
    -h --help               Show this screen.
    <command>               read/upload (required)
    --file=<filename>       Filename to post to blog.
    --search=<search_term>  Search latest blog containing term.
    -v --version            Show version.

"""

from docopt import docopt
from pyblog_functs import parse_args, create_creds, determine_call, \
                          WORDPRESS_USERNAME, WORDPRESS_PASSWORD


def main():
    """The main function - calls all other functions."""
    if __name__ == "__main__":
        arguments = docopt(__doc__, version='PyBlog 1.5.8')
    filename, command, search_term = parse_args(arguments)
    header = create_creds(WORDPRESS_USERNAME, WORDPRESS_PASSWORD)
    determine_call(header, command, filename, search_term)


main()
