#! /usr/bin/env python3
"""PyBlog
Usage:
    pyblog.py <command> [--file=<filename>]
    pyblog.py [--version]

Options:
    -h --help               Show this screen.
    <command>               read/upload (required)
    --file=<filename>       Filename to post to blog.
    -v --version            Show version.

"""

from pyblog_functs import parse_args, create_creds, determine_call, \
                          WORDPRESS_USERNAME, WORDPRESS_PASSWORD
from docopt import docopt


def main():
    """The main function - calls all other functions."""
    if __name__ == "__main__":
        arguments = docopt(__doc__, version='PyBlog 1.0')
    filename, command = parse_args(arguments)
    header = create_creds(WORDPRESS_USERNAME, WORDPRESS_PASSWORD)
    determine_call(header, command, filename)


main()
